const request = require('request-promise-native');

const url = 'https://www.healthcare.gov/api/glossary.json';

// return an array of healthcare.gov glossary items for the language specified
// if no language is specified, return an array of all glossary items.
exports.handler = async (language) => {
  const response = await request.get(url);
};
